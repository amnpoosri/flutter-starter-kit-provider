class Log {
  static void info(String message) {
    assert(message != null);

    print("ℹ $message");
  }

  static void warning(String message) {
    assert(message != null);

    print("⚠️ $message:");
  }

  static void exception(String message, dynamic exception) {
    assert(message != null);
    assert(exception != null);

    print("‼ $message: $exception");
  }

  static void error(dynamic exception) {
    assert(exception != null);

    print("!!! $exception");
  }
}
