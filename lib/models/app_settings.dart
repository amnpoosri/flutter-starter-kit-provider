import 'package:meta/meta.dart';

enum BuildType { Mock, Staging, Production }

@immutable
class AppSettings {
  /// App build type
  final BuildType buildType;

  /// API authority for all users - e.g. "www.domain.com",
  final String apiAuthority;

  /// API base URL - e.g. "/api"
  final String newApiBasePath;

  const AppSettings(
      {@required this.buildType,
      @required this.apiAuthority,
      @required this.newApiBasePath});
}
