class Drink {
  String id;
  String name;
  String category;
  String instructions;
  String strGlass;
  String strVideo;
  String dateModified;
  bool isFavorite;
  String imageUrl;
  String ingredient1;
  String ingredient2;
  String ingredient3;
  String ingredient4;
  String ingredient5;
  String ingredient6;
  String ingredient7;
  String ingredient8;
  String ingredient9;
  String ingredient10;
  String ingredient11;
  String ingredient12;
  String ingredient13;
  String ingredient14;
  String ingredient15;
  String strMeasure1;
  String strMeasure2;
  String strMeasure3;
  String strMeasure4;
  String strMeasure5;
  String strMeasure6;
  String strMeasure7;
  String strMeasure8;
  String strMeasure9;
  String strMeasure10;
  String strMeasure11;
  String strMeasure12;
  String strMeasure13;
  String strMeasure14;
  String strMeasure15;

  Drink(
      {this.id,
      this.name,
      this.category,
      this.instructions,
      this.strGlass,
      this.strVideo,
      this.dateModified,
      this.isFavorite,
      this.imageUrl,
      this.ingredient1,
      this.ingredient2,
      this.ingredient3,
      this.ingredient4,
      this.ingredient5,
      this.ingredient6,
      this.ingredient7,
      this.ingredient8,
      this.ingredient9,
      this.ingredient10,
      this.ingredient11,
      this.ingredient12,
      this.ingredient13,
      this.ingredient14,
      this.ingredient15,
      this.strMeasure1,
      this.strMeasure2,
      this.strMeasure3,
      this.strMeasure4,
      this.strMeasure5,
      this.strMeasure6,
      this.strMeasure7,
      this.strMeasure8,
      this.strMeasure9,
      this.strMeasure10,
      this.strMeasure11,
      this.strMeasure12,
      this.strMeasure13,
      this.strMeasure14,
      this.strMeasure15});

  Drink.fromJson(Map<String, dynamic> json)
      : id = json['idDrink'],
        name = json['strDrink'],
        category = json['strCategory'],
        instructions = json['strInstructions'],
        strGlass = json['strGlass'],
        strVideo = json['strVideo'],
        dateModified = json['dateModified'],
        isFavorite = json['isFavorite'],
        imageUrl = json['strDrinkThumb'],
        ingredient1 = json['strIngredient1'],
        ingredient2 = json['strIngredient2'],
        ingredient3 = json['strIngredient3'],
        ingredient4 = json['strIngredient4'],
        ingredient5 = json['strIngredient5'],
        ingredient6 = json['strIngredient6'],
        ingredient7 = json['strIngredient7'],
        ingredient8 = json['strIngredient8'],
        ingredient9 = json['strIngredient9'],
        ingredient10 = json['strIngredient10'],
        ingredient11 = json['strIngredient11'],
        ingredient12 = json['strIngredient12'],
        ingredient13 = json['strIngredient13'],
        ingredient14 = json['strIngredient14'],
        ingredient15 = json['strIngredient15'],
        strMeasure1 = json['strMeasure1'],
        strMeasure2 = json['strMeasure2'],
        strMeasure3 = json['strMeasure3'],
        strMeasure4 = json['strMeasure4'],
        strMeasure5 = json['strMeasure5'],
        strMeasure6 = json['strMeasure6'],
        strMeasure7 = json['strMeasure7'],
        strMeasure8 = json['strMeasure8'],
        strMeasure9 = json['strMeasure9'],
        strMeasure10 = json['strMeasure10'],
        strMeasure11 = json['strMeasure11'],
        strMeasure12 = json['strMeasure12'],
        strMeasure13 = json['strMeasure13'],
        strMeasure14 = json['strMeasure14'],
        strMeasure15 = json['strMeasure15'];
}
