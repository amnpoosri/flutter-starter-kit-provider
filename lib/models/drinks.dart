import 'drink.dart';

class Drinks {
  List<Drink> drinks;

  Drinks.fromJson(Map<String, dynamic> json)
      : drinks = json['drinks'] != null
            ? (json['drinks'] as List).map((i) => Drink.fromJson(i)).toList()
            : [];
}
