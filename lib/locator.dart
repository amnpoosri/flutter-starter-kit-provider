import 'package:get_it/get_it.dart';
import 'package:poc_template/models/app_settings.dart';
import 'package:poc_template/services/local_storage/local_storage_service.dart';
import 'package:poc_template/services/navigation/navigation_service.dart';
import 'package:poc_template/services/network/api_service.dart';

GetIt locator = GetIt.instance;

Future<void> setupLocator(AppSettings appSettings) async {
  // Register services
  var instance = await LocalStorageService.getInstance();
  locator.registerSingleton<LocalStorageService>(instance);

  locator.registerLazySingleton<ApiService>(
      () => ApiService(appSettings: appSettings));
  locator.registerLazySingleton<NavigationService>(() => NavigationService());
}
