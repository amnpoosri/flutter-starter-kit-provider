import 'package:flutter/material.dart';
import 'package:poc_template/app.dart';
import 'package:poc_template/locator.dart';
import 'package:poc_template/models/app_settings.dart';
import 'package:poc_template/viewmodels/app_vm.dart';
import 'package:provider/provider.dart';

const productionAppSettings = AppSettings(
  buildType: BuildType.Production,
  apiAuthority: "www.thecocktaildb.com",
  newApiBasePath: "/api/json/v1/1",
);

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();

  try {
    await setupLocator(productionAppSettings);
    runApp(MyApp());
  } catch (error) {
    print('Locator setup has failed');
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AppViewModel>(
        create: (context) => AppViewModel(), child: App());
  }
}
