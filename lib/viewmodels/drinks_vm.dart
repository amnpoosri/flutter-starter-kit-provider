import 'package:flutter/cupertino.dart';
import 'package:poc_template/constants/route_paths.dart';
import 'package:poc_template/locator.dart';
import 'package:poc_template/models/drink.dart';
import 'package:poc_template/services/navigation/navigation_service.dart';
import 'package:poc_template/services/network/api_service.dart';

class DrinksViewModel extends ChangeNotifier {
  final ApiService _apiService = locator<ApiService>();
  final NavigationService _navigationService = locator<NavigationService>();

  List<Drink> _drinks;
  List<Drink> get drinks => _drinks;

  bool _isGetDrinkLoading = false;
  bool get isGetDrinkLoading => _isGetDrinkLoading;

  String _currentSearchKeyword;
  String get currentSearchKeyword => _currentSearchKeyword;

  void setDrinks(List<Drink> value) {
    _drinks = value;
    notifyListeners();
  }

  void setIsGetDrinkLoading(bool value) {
    _isGetDrinkLoading = value;
    notifyListeners();
  }

  Future<void> getDrinks(String searchKeyword) async {
    setIsGetDrinkLoading(true);

    _currentSearchKeyword = searchKeyword;

    final result = await _apiService.getDrinks(searchKeyword);
    setIsGetDrinkLoading(false);

    if (result.isSuccess) {
      setDrinks(result.value.drinks);
    }
  }

  void goToDrinkDetailPage(Drink drink) async {
    await _navigationService.push(RoutePaths.DrinkDetail, arguments: drink);
  }
}
