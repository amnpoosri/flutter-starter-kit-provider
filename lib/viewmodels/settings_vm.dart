import 'package:flutter/cupertino.dart';
import 'package:poc_template/constants/route_paths.dart';
import 'package:poc_template/locator.dart';
import 'package:poc_template/services/local_storage/local_storage_service.dart';
import 'package:poc_template/services/navigation/navigation_service.dart';
import 'package:poc_template/viewmodels/app_vm.dart';
import 'package:provider/provider.dart';

class SettingsViewModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final LocalStorageService storageService = locator<LocalStorageService>();

  void updateDarkThemeSettings(BuildContext context, bool isDarkThemeEnable) {
    Provider.of<AppViewModel>(context, listen: false)
        .updateDarkThemeSettings(isDarkThemeEnable);
  }

  void logout() async {
    storageService.remove(LocalStorageService.SessionTokenKey);
    await _navigationService.pushNamedAndRemoveUntil(RoutePaths.Login);
  }
}
