import 'package:poc_template/constants/route_paths.dart';
import 'package:poc_template/locator.dart';
import 'package:poc_template/services/local_storage/local_storage_service.dart';
import 'package:poc_template/services/navigation/navigation_service.dart';

class LoginViewModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final LocalStorageService storageService = locator<LocalStorageService>();

  void onLoginTap() async {
    storageService.sessionToken = "Session Token";
    goToHomePage();
  }

  void goToHomePage() async {
   await _navigationService.pushNamedAndRemoveUntil(RoutePaths.Home);
  }
}
