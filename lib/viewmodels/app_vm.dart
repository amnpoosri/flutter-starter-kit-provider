import 'package:flutter/cupertino.dart';
import 'package:poc_template/locator.dart';
import 'package:poc_template/services/local_storage/local_storage_service.dart';

class AppViewModel extends ChangeNotifier {
  var storageService = locator<LocalStorageService>();

  bool _isDarkThemeEnable = false;
  bool get isDarkThemeEnable => _isDarkThemeEnable;

  AppViewModel() {
    _isDarkThemeEnable = storageService.darkTheme;
  }

  void updateDarkThemeSettings(bool isDarkThemeEnable) {
    _isDarkThemeEnable = isDarkThemeEnable;
    storageService.darkTheme = isDarkThemeEnable;
    notifyListeners();
  }
}
