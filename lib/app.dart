import 'package:flutter/material.dart';
import 'package:poc_template/constants/route_paths.dart';
import 'package:poc_template/custom/custom_themes.dart';
import 'package:poc_template/locator.dart';
import 'package:poc_template/router/router.dart' as router;
import 'package:poc_template/services/local_storage/local_storage_service.dart';
import 'package:poc_template/services/navigation/navigation_service.dart';
import 'package:poc_template/viewmodels/app_vm.dart';
import 'package:provider/provider.dart';

import 'custom/custom_strings.dart';

class App extends StatelessWidget {
  final storageService = locator<LocalStorageService>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: CustomStrings.appName,
      theme: Provider.of<AppViewModel>(context).isDarkThemeEnable
          ? CustomTheme.darkTheme
          : CustomTheme.lightTheme,
      navigatorKey: locator<NavigationService>().navigatorKey,
      onGenerateRoute: router.generateRoute,
      initialRoute: storageService.sessionToken == null ? RoutePaths.Login : RoutePaths.Home,
    );
  }
}
