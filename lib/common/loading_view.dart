import 'package:flutter/material.dart';
import '../custom/custom_colors.dart';

class LoadingView extends StatelessWidget {
  final String title;
  final bool isLoading;

  // Passed in from Container
  LoadingView({
    @required this.title,
    @required this.isLoading,
  });

  @override
  Widget build(BuildContext context) {
    return isLoading
        ?
        // Loading
        Container(
            child: Center(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  child: CircularProgressIndicator(),
                  height: 48.0,
                  width: 48.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24.0,
                      color: CustomColors.white,
                    ),
                  ),
                )
              ],
            )),
            color: CustomColors.black.withOpacity(0.25),
          )
        : Container();
  }
}
