import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:poc_template/custom/custom_colors.dart';

class RoundedButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final String title;

  RoundedButton({this.onTap, @required this.title});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(8.0),
      highlightColor: CustomColors.white,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 38.0, vertical: 12.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color: onTap != null ? CustomColors.red : CustomColors.lightGrey,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AutoSizeText(
              title,
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18.0,
                color: onTap != null ? CustomColors.white : CustomColors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
