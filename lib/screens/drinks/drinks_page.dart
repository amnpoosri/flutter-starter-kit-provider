import 'dart:async';

import 'package:flutter/material.dart';
import 'package:poc_template/custom/custom_colors.dart';
import 'package:poc_template/custom/custom_strings.dart';
import 'package:poc_template/models/drink.dart';
import 'package:poc_template/viewmodels/app_vm.dart';
import 'package:poc_template/viewmodels/drinks_vm.dart';
import 'package:provider/provider.dart';

class DrinksPage extends StatefulWidget {
  @override
  _DrinksPageState createState() => _DrinksPageState();
}

class _DrinksPageState extends State<DrinksPage> {
  double screenWidth;
  TextEditingController _searchTextController = TextEditingController();
  Timer _searchCocktailDebounce;

  @override
  void initState() {
    super.initState();
    final searchKeyword = Provider.of<DrinksViewModel>(context, listen: false)
        .currentSearchKeyword;
    _searchTextController = TextEditingController(text: searchKeyword);
  }

  void dispose() {
    _searchTextController.dispose();
    super.dispose();
  }

  Widget _buildDrinkList(List<Drink> drinks) {
    return Flexible(
      fit: FlexFit.loose,
      child: ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: drinks.length,
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return _buildDrinkItem(drinks[index],
                  padding: EdgeInsets.only(bottom: 8, right: 8, left: 8));
            }
            return _buildDrinkItem(drinks[index]);
          }),
    );
  }

  Widget _buildFieldAndValueText(String field, String title) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        if (field != null)
          Text(
            '$field : ',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
            overflow: TextOverflow.ellipsis,
          ),
        Expanded(
          child: Text(
            title,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 18),
          ),
        ),
      ],
    );
  }

  Widget _buildDrinkItem(Drink drink, {EdgeInsets padding}) {
    return Card(
      margin: padding ?? EdgeInsets.all(8),
      shape: BeveledRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: InkWell(
        onTap: () =>
            Provider.of<DrinksViewModel>(context, listen: false).goToDrinkDetailPage(drink),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius:
                      BorderRadius.only(topLeft: const Radius.circular(4.0)),
                  child: Hero(
                    tag: 'Drink: ${drink.id}',
                    child: Image.network(
                      drink.imageUrl,
                      fit: BoxFit.cover,
                      height: 120,
                      width: screenWidth / 3,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 120,
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _buildFieldAndValueText(CustomStrings.name, drink.name),
                        _buildFieldAndValueText(CustomStrings.category, drink.category),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          child: Text(
                            '${drink.instructions}',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            _buildIngredientHeader(),
            if (drink.ingredient1 != null)
              _buildIngredientItem('1.', drink.ingredient1, drink.strMeasure1),
            if (drink.ingredient2 != null)
              _buildIngredientItem('2.', drink.ingredient2, drink.strMeasure2),
            if (drink.ingredient3 != null)
              _buildIngredientItem('3.', drink.ingredient3, drink.strMeasure3),
            if (drink.ingredient4 != null)
              _buildIngredientItem('4.', drink.ingredient4, drink.strMeasure4),
            if (drink.ingredient5 != null)
              _buildIngredientItem('5.', drink.ingredient5, drink.strMeasure5),
            if (drink.ingredient6 != null)
              _buildIngredientItem('6.', drink.ingredient6, drink.strMeasure6),
            if (drink.ingredient7 != null)
              _buildIngredientItem('7.', drink.ingredient7, drink.strMeasure7),
            if (drink.ingredient8 != null)
              _buildIngredientItem('8.', drink.ingredient8, drink.strMeasure8),
            if (drink.ingredient9 != null)
              _buildIngredientItem('9.', drink.ingredient9, drink.strMeasure9),
            if (drink.ingredient10 != null)
              _buildIngredientItem(
                  '10.', drink.ingredient10, drink.strMeasure10),
            if (drink.ingredient11 != null)
              _buildIngredientItem(
                  '11.', drink.ingredient11, drink.strMeasure11),
            if (drink.ingredient12 != null)
              _buildIngredientItem(
                  '12.', drink.ingredient12, drink.strMeasure12),
            if (drink.ingredient13 != null)
              _buildIngredientItem(
                  '13.', drink.ingredient13, drink.strMeasure13),
            if (drink.ingredient14 != null)
              _buildIngredientItem(
                  '14.', drink.ingredient14, drink.strMeasure14),
            if (drink.ingredient15 != null)
              _buildIngredientItem(
                  '15.', drink.ingredient15, drink.strMeasure15),
          ],
        ),
      ),
    );
  }

  Widget _buildIngredientHeader() {
    return Padding(
      padding: EdgeInsets.only(top: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Text(
              CustomStrings.no,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
            flex: 1,
          ),
          Expanded(
            child: Text(
              CustomStrings.ingredients,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
            flex: 6,
          ),
          Expanded(
            child: Text(
              CustomStrings.quantity,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
            flex: 3,
          ),
        ],
      ),
    );
  }

  Widget _buildIngredientItem(
      String number, String ingredient, String quantity) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Text(
              number ?? '',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            flex: 1,
          ),
          Expanded(
            child: Text(
              ingredient ?? '-',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            flex: 6,
          ),
          Expanded(
            child: Text(
              quantity ?? '-',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            flex: 3,
          ),
        ],
      ),
    );
  }

  Widget _buildEmptyCocktailView() {
    return Column(
      children: <Widget>[
        Icon(Icons.mood_bad),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            CustomStrings.cannotFindCocktails,
            style: TextStyle(fontSize: 16),
          ),
        )
      ],
    );
  }

  Widget _buildSearchBar() {
    return Container(
      padding: EdgeInsets.all(16),
      child: TextField(
        controller: _searchTextController,
        onChanged: (value) {
          if (_searchCocktailDebounce?.isActive ?? false)
            _searchCocktailDebounce.cancel();
          _searchCocktailDebounce =
              Timer(const Duration(milliseconds: 400), () {
            Provider.of<DrinksViewModel>(context, listen: false).getDrinks(value);
          });
        },
        style: TextStyle(color: CustomColors.black),
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: CustomStrings.searchHint,
          hintStyle: TextStyle(color: CustomColors.grey),
          prefixIcon: Icon(Icons.search, color: CustomColors.black),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          filled: true,
          contentPadding: EdgeInsets.all(16),
          fillColor: CustomColors.white,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    return Consumer<DrinksViewModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          height: double.infinity,
          color: !Provider.of<AppViewModel>(context).isDarkThemeEnable
              ? CustomColors.alto
              : null,
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _buildSearchBar(),
                if (model.isGetDrinkLoading)
                  SizedBox(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(CustomColors.red),
                    ),
                    height: 48.0,
                    width: 48.0,
                  )
                else if (model.drinks == null || model.drinks.isEmpty)
                  _buildEmptyCocktailView()
                else
                  _buildDrinkList(model.drinks)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
