import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poc_template/assets/image_assets.dart';
import 'package:poc_template/common/rounded_button.dart';
import 'package:poc_template/custom/custom_colors.dart';
import 'package:poc_template/custom/custom_strings.dart';
import 'package:poc_template/viewmodels/app_vm.dart';
import 'package:poc_template/viewmodels/login_vm.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Widget _buildLogo() {
    return AutoSizeText(
      CustomStrings.appName,
      style: TextStyle(
          fontFamily: 'Courgette',
          color: CustomColors.red,
          fontSize: 60,
          fontWeight: FontWeight.w600),
    );
  }

  Widget _buildDrinkingImage() {
    return Padding(
      padding: EdgeInsets.only(top: 32),
      child: Image.asset(
        ImageAssets.drinking,
        height: 180,
        color: Provider.of<AppViewModel>(context).isDarkThemeEnable
            ? CustomColors.white
            : CustomColors.black,
      ),
    );
  }

  Widget _buildBrandDescription() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 32),
      child: Text(
        CustomStrings.appDescription,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontFamily: 'Courgette', color: CustomColors.silver, fontSize: 24),
      ),
    );
  }

  Widget _buildLoginButton() {
    return RoundedButton(
      title: CustomStrings.login,
      onTap: () =>
          Provider.of<LoginViewModel>(context, listen: false).onLoginTap(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 32),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _buildLogo(),
                _buildDrinkingImage(),
                _buildBrandDescription(),
                _buildLoginButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
