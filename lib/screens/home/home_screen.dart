import 'package:flutter/material.dart';
import 'package:poc_template/custom/custom_colors.dart';
import 'package:poc_template/custom/custom_strings.dart';
import 'package:poc_template/screens/drinks/drinks_page.dart';
import 'package:poc_template/screens/settings/settings.dart';
import 'package:poc_template/viewmodels/drinks_vm.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[DrinksPage(), SettingsPage()];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    Future.microtask(() =>
        Provider.of<DrinksViewModel>(context, listen: false).getDrinks("")
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(CustomStrings.myCocktail),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.local_drink),
            title: Text(CustomStrings.drinks),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text(CustomStrings.settings),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: CustomColors.red,
        onTap: _onItemTapped,
      ),
    );
  }
}
