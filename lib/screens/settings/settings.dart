import 'package:flutter/material.dart';
import 'package:poc_template/custom/custom_strings.dart';
import 'package:poc_template/viewmodels/app_vm.dart';
import 'package:poc_template/viewmodels/settings_vm.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  Widget _buildSettingTile(Icon leadingIcon, String title, bool switchValue,
      Function onSwitchValueChanged) {
    return ListTile(
      leading: leadingIcon,
      title: Text(title),
      trailing: Switch(value: switchValue, onChanged: onSwitchValueChanged),
    );
  }

  Widget _buildMenuTile(Icon leadingIcon, String title, Function onTap) {
    return ListTile(
      leading: leadingIcon,
      title: Text(title),
      trailing: Icon(Icons.keyboard_arrow_right),
      onTap: onTap,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _buildSettingTile(
              Icon(Icons.opacity),
              CustomStrings.darkTheme,
              Provider.of<AppViewModel>(context).isDarkThemeEnable,
              (value) => Provider.of<SettingsViewModel>(context, listen: false)
                  .updateDarkThemeSettings(context, value)),
          _buildMenuTile(Icon(Icons.lock), CustomStrings.logout,
              () => Provider.of<SettingsViewModel>(context, listen: false).logout()),
        ],
      ),
    );
  }
}
