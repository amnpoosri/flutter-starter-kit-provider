import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poc_template/custom/custom_strings.dart';
import 'package:poc_template/models/drink.dart';

class DrinkDetailScreen extends StatelessWidget {
  final Drink drink;

  const DrinkDetailScreen({Key key, this.drink}) : super(key: key);

  Widget _buildDrinkImage(String imageUrl, String imageTag) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
        child: Hero(
          tag: imageTag,
          child: Image.network(
            imageUrl,
            fit: BoxFit.cover,
            height: 150,
            width: 150,
          ),
        ),
      ),
    );
  }

  Widget _buildFieldAndValueText(String field, String title) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        if (field != null)
          Text(
            '$field : ',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
            overflow: TextOverflow.ellipsis,
          ),
        Expanded(
          child: Text(
            title,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 18),
          ),
        ),
      ],
    );
  }

  Widget _buildDrinkInformation(Drink drink) {
    return Column(
      children: <Widget>[
        _buildFieldAndValueText(CustomStrings.name, drink.name),
        _buildFieldAndValueText(CustomStrings.category, drink.category),
      ],
    );
  }

  Widget _buildSectionDescription(BuildContext context, String description) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Text(
        description,
        textAlign: TextAlign.start,
        style: TextStyle(fontSize: 16),
      ),
    );
  }

  Widget _buildSectionTitle(String title) {
    return Padding(
      padding: EdgeInsets.only(bottom: 8),
      child: Text(
        title,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
      ),
    );
  }

  Widget _buildSection(String sectionTitle, Widget content) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_buildSectionTitle(sectionTitle), content],
      ),
    );
  }

  Widget _buildIngredientHeader() {
    return Padding(
      padding: EdgeInsets.only(top: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Text(
              CustomStrings.no,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
            flex: 1,
          ),
          Expanded(
            child: Text(
              CustomStrings.ingredients,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
            flex: 6,
          ),
          Expanded(
            child: Text(
              CustomStrings.quantity,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
            flex: 3,
          ),
        ],
      ),
    );
  }

  Widget _buildIngredientItem(
      String number, String ingredient, String quantity) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Text(
              number ?? '',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            flex: 1,
          ),
          Expanded(
            child: Text(
              ingredient ?? '-',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            flex: 6,
          ),
          Expanded(
            child: Text(
              quantity ?? '-',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            flex: 3,
          ),
        ],
      ),
    );
  }

  Widget _buildDrinkRecipe(Drink drink) {
    return Column(
      children: <Widget>[
        _buildIngredientHeader(),
        if (drink.ingredient1 != null)
          _buildIngredientItem('1.', drink.ingredient1, drink.strMeasure1),
        if (drink.ingredient2 != null)
          _buildIngredientItem('2.', drink.ingredient2, drink.strMeasure2),
        if (drink.ingredient3 != null)
          _buildIngredientItem('3.', drink.ingredient3, drink.strMeasure3),
        if (drink.ingredient4 != null)
          _buildIngredientItem('4.', drink.ingredient4, drink.strMeasure4),
        if (drink.ingredient5 != null)
          _buildIngredientItem('5.', drink.ingredient5, drink.strMeasure5),
        if (drink.ingredient6 != null)
          _buildIngredientItem('6.', drink.ingredient6, drink.strMeasure6),
        if (drink.ingredient7 != null)
          _buildIngredientItem('7.', drink.ingredient7, drink.strMeasure7),
        if (drink.ingredient8 != null)
          _buildIngredientItem('8.', drink.ingredient8, drink.strMeasure8),
        if (drink.ingredient9 != null)
          _buildIngredientItem('9.', drink.ingredient9, drink.strMeasure9),
        if (drink.ingredient10 != null)
          _buildIngredientItem('10.', drink.ingredient10, drink.strMeasure10),
        if (drink.ingredient11 != null)
          _buildIngredientItem('11.', drink.ingredient11, drink.strMeasure11),
        if (drink.ingredient12 != null)
          _buildIngredientItem('12.', drink.ingredient12, drink.strMeasure12),
        if (drink.ingredient13 != null)
          _buildIngredientItem('13.', drink.ingredient13, drink.strMeasure13),
        if (drink.ingredient14 != null)
          _buildIngredientItem('14.', drink.ingredient14, drink.strMeasure14),
        if (drink.ingredient15 != null)
          _buildIngredientItem('15.', drink.ingredient15, drink.strMeasure15),
      ],
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(drink.name),
      ),
      body: SingleChildScrollView(
        primary: true,
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildDrinkImage(drink.imageUrl, CustomStrings.drink + drink.id),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(top: 16, right: 16),
                        child: _buildDrinkInformation(drink))),
              ],
            ),
            _buildSection(CustomStrings.instruction,
                _buildSectionDescription(context, drink.instructions)),
            _buildSection(
                CustomStrings.glass, _buildSectionDescription(context, drink.strGlass)),
            _buildSection(CustomStrings.recipe, _buildDrinkRecipe(drink)),
          ],
        ),
      ),
    );
  }
}
