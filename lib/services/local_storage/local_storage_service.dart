import 'package:poc_template/utils/log.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {
  static LocalStorageService _instance;
  static SharedPreferences _preferences;

  static Future<LocalStorageService> getInstance() async {
    if (_instance == null) {
      _instance = LocalStorageService();
    }

    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }

    return _instance;
  }

  static const String SessionTokenKey = 'sessionToken';
  static const String DarkThemeKey = 'darkTheme';

  // Session Token
  String get sessionToken => _getFromDisk(SessionTokenKey);

  set sessionToken(String sessionToken) =>
      _saveToDisk(SessionTokenKey, sessionToken);

  // Dark Theme
  bool get darkTheme => _getFromDisk(DarkThemeKey) ?? false;
  set darkTheme(bool value) => _saveToDisk(DarkThemeKey, value);

  dynamic _getFromDisk(String key) {
    var value = _preferences.get(key);
    return value;
  }

  void remove(String key) {
    _preferences.remove(key);
  }

  // Helper function to save data to local storage
  void _saveToDisk<T>(String key, T content) {
    Log.info(
        '(TRACE) LocalStorageService:_saveToDisk. key: $key value: $content');

    if (content is String) {
      _preferences.setString(key, content);
    }
    if (content is bool) {
      _preferences.setBool(key, content);
    }
    if (content is int) {
      _preferences.setInt(key, content);
    }
    if (content is double) {
      _preferences.setDouble(key, content);
    }
    if (content is List<String>) {
      _preferences.setStringList(key, content);
    }
  }
}
