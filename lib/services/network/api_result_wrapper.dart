enum ErrorType { timeout, server, other, none }

class ApiResultWrapper<T> {
  T data;
  Error error;

  ApiResultWrapper({this.data, this.error});

  ApiResultWrapper.fromJson(
      Map<String, dynamic> json, T Function(dynamic) createModel)
      : data = createModel(json),
        error = null;
}

class Error {
  String id;
  String code;
  String data;
  ErrorType errorType;

  Error({this.id, this.code, this.data, this.errorType = ErrorType.none});

  Error.fromJson(Map<String, dynamic> map) {
    id = map['id'];
    code = map['code'];
    data = map['data'];
    errorType = ErrorType.server;
  }
}
