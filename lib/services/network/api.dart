import 'package:poc_template/models/drinks.dart';
import 'package:poc_template/services/network/api_result.dart';

abstract class Api {
  Future<ApiResult<Drinks>> getDrinks(String searchKeyword);
}
