import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:poc_template/models/app_settings.dart';
import 'package:poc_template/models/drinks.dart';
import 'package:poc_template/services/network/api.dart';
import 'package:poc_template/services/network/api_networking.dart';
import 'package:poc_template/services/network/api_result.dart';

class ApiService extends Api with ApiNetworking {
  AppSettings appSettings;

  ApiService({this.appSettings}) {
    assert(appSettings != null);
  }

  Uri makeUrl(String path, [Map<String, String> queryParameters]) {
    return Uri.https(appSettings.apiAuthority,
        appSettings.newApiBasePath + path, queryParameters);
  }

  @override
  Future<ApiResult<Drinks>> getDrinks(String searchKeyword) => get(
        path: '/search.php',
        queryParameters: {"s": searchKeyword},
        createModel: (json) => Drinks.fromJson(json),
      );

  static Map<String, dynamic> decodeJson(http.Response response) {
    final body = utf8.decode(response.bodyBytes);
    return json.decode(body);
  }
}
