import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:poc_template/models/app_settings.dart';
import 'package:poc_template/services/network/api.dart';
import 'package:poc_template/services/network/api_result.dart';
import 'package:poc_template/services/network/api_result_wrapper.dart';
import 'package:poc_template/services/network/logging_interceptor.dart';
import 'package:poc_template/utils/log.dart';

mixin ApiNetworking on Api {
  static final Dio _dio = Dio()
    ..interceptors.add(LoggingInterceptor())
    //Connection timeout 10s
    ..options.connectTimeout = 10000
    ..options.receiveTimeout = 10000
    // By default Dio throws an exception for HTTP codes such as 403
    // but we want to handle these ourselves
    ..options.validateStatus = (_) => true;

  AppSettings get appSettings;

  String get authority => appSettings.apiAuthority;

  String get newApiBasePath => appSettings.newApiBasePath;

  @visibleForTesting
  void setHttpClientAdapter(HttpClientAdapter adapter) {
    _dio.httpClientAdapter = adapter;
  }

  bool _isTimeoutError(dynamic e) {
    if (e is DioError) {
      if (e.type == DioErrorType.RECEIVE_TIMEOUT ||
          e.type == DioErrorType.CONNECT_TIMEOUT ||
          e.type == DioErrorType.SEND_TIMEOUT) {
        return true;
      }
    }
    return false;
  }

  // Implemented in api_service.dart
  Uri makeUrl(String path, [Map<String, String> queryParameters]);

  /// Get and deseralize and JSON object specified by [T] parameter
  /// [createModel] - a function which creates a model from JSON
  Future<ApiResult<T>> get<T>({
    @required String path,
    @required T Function(dynamic) createModel,
    Map<String, String> queryParameters,
  }) async {
    try {
      final Function doRequest = () async {
        final headers = await _buildRequestHeaders();

        return await _dio.getUri<List<int>>(makeUrl(path, queryParameters),
            options:
                Options(responseType: ResponseType.bytes, headers: headers));
      };

      var response = await doRequest();

      if (_didRequestFail(response)) {
        final result = _deserializeJson(response, createModel);
        return ApiResult.failure(result, response.statusCode);
      }

      final result = _deserializeJson(response, createModel);
      return ApiResult.success(result.data);
    } catch (e) {
      Log.exception("Exception thrown on GET to $path", e);
      if (_isTimeoutError(e)) {
        return ApiResult.failure(
            ApiResultWrapper(error: Error(errorType: ErrorType.timeout)));
      }
      return ApiResult.failure(
          ApiResultWrapper(error: Error(errorType: ErrorType.other)));
    }
  }

  Future<ApiResult<T>> post<T>({
    @required String path,
    dynamic postData,
    T Function(dynamic) createModel,
    bool allowUnauthorizedResponse = false,
    Map<String, dynamic> apiHeaders,
    Map<String, String> queryParameters,
  }) async {
    if (createModel == null) {
      createModel = (_) => null;
    }

    try {
      final Function doRequest = () async {
        final headers = apiHeaders ?? await _buildRequestHeaders();

        return await _dio.postUri<List<int>>(makeUrl(path, queryParameters),
            data: postData,
            options:
                Options(responseType: ResponseType.bytes, headers: headers));
      };

      var response = await doRequest();

      if (_didRequestFail(response)) {
        final result = _deserializeJson(response, createModel);
        return ApiResult.failure(result, response.statusCode);
      }

      final result = _deserializeJson(response, createModel);
      return ApiResult.success(result.data);
    } catch (e) {
      if (_isTimeoutError(e)) {
        return ApiResult.failure(
            ApiResultWrapper(error: Error(errorType: ErrorType.timeout)));
      }
      return ApiResult.failure(
          ApiResultWrapper(error: Error(errorType: ErrorType.other)));
    }
  }

  static Future<Map<String, dynamic>> _buildRequestHeaders() async {
    final headers = Map<String, dynamic>();

    headers[HttpHeaders.contentTypeHeader] = "application/json";
    return headers;
  }

  static _deserializeJson<T>(
      Response<List<int>> response, T Function(dynamic) createModel) {
    final decoded = json.decode(utf8.decode(response.data));
    final resultWrapper = ApiResultWrapper<T>.fromJson(decoded, createModel);
    return resultWrapper;
  }

  /// For debugging, allows proxying requests through e.g. Charles Proxy
  // ignore: unused_element
  void debugSetupProxy(String proxy) {
    final adapter = _dio.httpClientAdapter as DefaultHttpClientAdapter;
    adapter.onHttpClientCreate = (HttpClient client) {
      client.findProxy = (uri) => "PROXY $proxy";
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
    };
  }

  static _didRequestFail(Response response) {
    return response.statusCode >= 400;
  }
}
