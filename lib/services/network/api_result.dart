import 'package:dio/dio.dart';

/// Wrapper for an API result to defend against failed requests.
/// Consuming code should always check [isSuccess] property before
/// accessing [value].
class ApiResult<T> {
  /// True if the server returned a result with a HTTP response code
  /// less than 400.
  bool isSuccess;

  /// Raw HTTP response object if the server sent one, or an exception
  /// if something went wrong.
  final dynamic result;

  int statusCode;

  /// Deserialized data.
  T _value;
  T get value {
    assert(
        isSuccess, "ApiResult: value property accessed when isSuccess=false");
    return _value;
  }

  /// Successful request with deserialized object and optional raw
  /// HTTP response object
  ApiResult.success(this._value, [this.result]) {
    if (this.result is Response) {
      statusCode = (this.result as Response).statusCode;
    }

    isSuccess = true;
  }

  /// Failed request constructor
  static ApiResult<T> failure<T>([dynamic response, int statusCode]) {
    return ApiResult<T>(response, statusCode);
  }

  /// Only used internally by this class for failed request.
  /// Consumers should use ApiResult.failure().
  ApiResult(this.result, this.statusCode) {
    _value = null;
    isSuccess = false;
    statusCode = statusCode;
  }
}
