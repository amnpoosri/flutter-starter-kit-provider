import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class LoggingInterceptor extends Interceptor {
  bool _loggingEnabled = false;

  LoggingInterceptor() {
    assert(() {
      // Only enable logging in debug builds
      _loggingEnabled = true;
      return true;
    }());
  }

  @override
  onRequest(RequestOptions options) {
    if (!_loggingEnabled) {
      return Future.value(null);
    }

    var log = "\n⬆️ ${options.method} ${options.uri}\n";
    options.headers.forEach((k, v) => log += "   $k: $v\n");
    log += "\n";

    if (options.data != null) {
      log += "   " + options.data.toString();
    }

    debugPrint(log);
    return Future.value(null);
  }

  @override
  onResponse(Response response) {
    if (!_loggingEnabled) {
      return Future.value(null);
    }

    final emoji = (response.statusCode < 400) ? "✅" : "❌";
    var log =
        "\n$emoji ${response.request.method} ${response.request.uri} returned ${response.statusCode} ${response.statusMessage}\n";
    response.headers.forEach((k, v) => log += "   $k: $v\n");
    log += "\n";

    if (response.data != null && response.data is List<int>) {
      log += "   " + utf8.decode(response.data);
    }

    debugPrint(log);
    return Future.value(null);
  }
}
