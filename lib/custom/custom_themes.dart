import 'package:flutter/material.dart';
import 'package:poc_template/custom/custom_colors.dart';

class CustomTheme {
  static ThemeData lightTheme = ThemeData(
    backgroundColor: CustomColors.white,
    primaryColor: CustomColors.white,
    accentColor: CustomColors.white,
    scaffoldBackgroundColor: CustomColors.white,
    appBarTheme: AppBarTheme(
      textTheme: TextTheme(
        title: TextStyle(
          color: CustomColors.black,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
      ),
    ),
  );

  static ThemeData darkTheme = ThemeData(
    brightness: Brightness.dark,
    backgroundColor: CustomColors.black,
    primaryColor: CustomColors.black,
    accentColor: CustomColors.black,
    scaffoldBackgroundColor: CustomColors.black,
    appBarTheme: AppBarTheme(
      textTheme: TextTheme(
        title: TextStyle(
          color: CustomColors.white,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
      ),
    ),
  );
}
