class CustomStrings {
  static const String appName = "MyCocktails";
  static const String appDescription =
      "No more memorizing your favorite cocktail ingredients. MyCocktails has collection of tonnes of cocktail recipes around the world.";
  static const String myCocktail = "My Cocktails";
  static const String name = "Name";
  static const String category = "Category";
  static const String drink = "Drink: ";
  static const String drinks = "Drinks";
  static const String settings = "Settings";
  static const String no = "No.";
  static const String ingredients = "Ingredients";
  static const String quantity = "Quantity";
  static const String instruction = "Instruction";
  static const String glass = "Glass";
  static const String recipe = "Recipe";
  static const String undefinedRoute = "Undefine route ";
  static const String cannotFindCocktails = "Can't find any cocktails.";
  static const String login = "LOGIN";
  static const String searchHint = "Search cocktails...";
  static const String darkTheme = "Dark Theme";
  static const String logout = "Logout";
}