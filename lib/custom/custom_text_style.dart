import 'dart:ui';

import 'custom_colors.dart';

class CustomTextStyle extends TextStyle {
  static const FontWeight light = FontWeight.w200;
  static const FontWeight regular = FontWeight.w400;
  static const FontWeight medium = FontWeight.w600;
  static const FontWeight semibold = FontWeight.w700;
  static const FontWeight bold = FontWeight.w900;

  CustomTextStyle.makeFont({
    double fontSize,
    FontWeight fontWeight,
    Color color,
  }) : super(
          color: color ?? CustomColors.black,
          fontSize: fontSize,
          fontWeight: fontWeight ?? CustomTextStyle.regular,
          textBaseline: TextBaseline.alphabetic,
        );
}

TextStyle mkFont(double fontSize, Color color) =>
    CustomTextStyle.makeFont(fontSize: fontSize, color: color);

TextStyle mkFontSize(double fontSize) =>
    CustomTextStyle.makeFont(fontSize: fontSize);

TextStyle mkFontColor(Color color) => CustomTextStyle.makeFont(color: color);

TextStyle mkFontLight(double fontSize, [Color color]) =>
    CustomTextStyle.makeFont(
      fontSize: fontSize,
      fontWeight: CustomTextStyle.light,
      color: color ?? CustomColors.black,
    );
TextStyle mkFontRegular(double fontSize, [Color color]) =>
    CustomTextStyle.makeFont(
      fontSize: fontSize,
      fontWeight: CustomTextStyle.regular,
      color: color ?? CustomColors.black,
    );
TextStyle mkFontMedium(double fontSize, [Color color]) =>
    CustomTextStyle.makeFont(
      fontSize: fontSize,
      fontWeight: CustomTextStyle.medium,
      color: color ?? CustomColors.black,
    );
TextStyle mkFontSemi(double fontSize, [Color color]) =>
    CustomTextStyle.makeFont(
      fontSize: fontSize,
      fontWeight: CustomTextStyle.semibold,
      color: color ?? CustomColors.black,
    );
TextStyle mkFontBold(double fontSize, [Color color]) =>
    CustomTextStyle.makeFont(
      fontSize: fontSize,
      fontWeight: CustomTextStyle.bold,
      color: color ?? CustomColors.black,
    );
