import 'package:flutter/material.dart';

class CustomColors {
  static const alto = const Color(0xFFDAD9D9);
  static const red = const Color(0xFFFF0000);
  static const orange = const Color(0xFFFF7D00);
  static const green = const Color(0xFF99CC00);
  static const darkGreen = const Color(0xFF99CC00);
  static const limeade = const Color(0xFF7DA600);
  static const lightGreen = const Color(0xFFf7fff8);
  static const black = const Color(0xFF000000);
  static const darkGrey = const Color(0xFF4C4C4C);
  static const grey = const Color(0xFF969696);
  static const silver = const Color(0xFFB8B8B8);
  static const lightGrey = const Color(0xFFEAEAEA);
  static const veryLightGrey = const Color(0xFFF2F2F2);
  static const white = const Color(0xFFFFFFFF);
}
