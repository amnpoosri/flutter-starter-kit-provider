import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:poc_template/constants/route_paths.dart';
import 'package:poc_template/custom/custom_strings.dart';
import 'package:poc_template/models/drink.dart';
import 'package:poc_template/screens/drink_detail/drink_detail_screen.dart';
import 'package:poc_template/screens/home/home_screen.dart';
import 'package:poc_template/screens/login/login_screen.dart';
import 'package:poc_template/viewmodels/drinks_vm.dart';
import 'package:poc_template/viewmodels/home_vm.dart';
import 'package:poc_template/viewmodels/login_vm.dart';
import 'package:poc_template/viewmodels/settings_vm.dart';
import 'package:provider/provider.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case RoutePaths.Home:
      return CupertinoPageRoute<bool>(
          builder: (BuildContext context) => MultiProvider(
                providers: [
                  Provider<HomeViewModel>(create:(_) => HomeViewModel()),
                  Provider<SettingsViewModel>(create:(_) =>  SettingsViewModel()),
                  ChangeNotifierProvider<DrinksViewModel>(create:(_) => DrinksViewModel()),
                ],
                child: HomePage(),
              ));
    case RoutePaths.Login:
      return MaterialPageRoute(
          builder: (_) => Provider<LoginViewModel>(
              create: (_) => LoginViewModel(), child: LoginPage()));

    case RoutePaths.DrinkDetail:
      Drink drink = settings.arguments as Drink;

      return CupertinoPageRoute<bool>(
          builder: (BuildContext context) => DrinkDetailScreen(drink: drink));

    default:
      return MaterialPageRoute(
          builder: (_) => Scaffold(
                body: Center(
                  child: Text(CustomStrings.undefinedRoute + settings.name),
                ),
              ));
  }
}
